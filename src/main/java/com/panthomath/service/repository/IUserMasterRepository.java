package com.panthomath.service.repository;

import org.springframework.data.repository.CrudRepository;

import com.panthomath.service.model.TbUserMaster;

public interface IUserMasterRepository extends CrudRepository<TbUserMaster, Long>{


	TbUserMaster findBynUserId(Long nUserId);
	
	
	
}
