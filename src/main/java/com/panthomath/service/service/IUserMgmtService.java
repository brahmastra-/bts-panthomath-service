package com.panthomath.service.service;

import org.springframework.stereotype.Service;

import com.panthomath.service.dto.InputUserDetailsDto;
import com.panthomath.service.dto.OutputResponseDto;


@Service
public interface IUserMgmtService {
	public OutputResponseDto getAllUserDetails(Long nUserId);

	public OutputResponseDto registerUserDetails(InputUserDetailsDto inputUserDetailsDto);


}
